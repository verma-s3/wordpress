<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <link rel="stylesheet" type="text/css" href="/wp-content/themes/first_theme/style.css">
  <title><?php echo get_bloginfo(); ?></title>
  <?php wp_head(); ?>
</head>
<body>
	<div id="container">
		<header>
			<h1><?php echo get_bloginfo(); ?></h1>
		</header>