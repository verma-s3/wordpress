<?php
	get_header();
?>
<section>
	
	<div id="inner">
		<div id="main">
			

			

			<?php while(have_posts()) : ?>
				<?php the_post(); ?>


				<?php if(is_page() || is_single()): ?>
					<h2><?php the_title(); ?></h2>
				<?php else: ?>
					<h1><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h1>
				<?php endif; ?>


				<?php if(is_single()): ?>
					<p><small>Posted on <?php echo get_the_date('',$post->ID) ?>, by <?php the_author(); ?></small></p>
					<p>Posted in <?php the_category(',') ?></p>
				<?php endif; ?>


				<?php if(is_page() || is_single()): ?>
					<?php the_content(); ?>
				<?php else: ?>
					<?php the_excerpt(); ?>
				<?php endif; ?>

			<?php endwhile; ?>
		</div>
		<div id="secondary">
			<h2>Menu</h2>
			<!-- nav menu -->
			
			<?php wp_nav_menu(); ?>
			
			<h2>Archive</h2>
			<!-- getting the blogs  -->
			<ul>
				<?php wp_get_archives(); ?> 
			</ul>
		</div>
	</div>
	
</section>
<?php
	get_footer();
?>