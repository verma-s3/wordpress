<?php
	get_header();
?>
<section>
	
	<div id="inner">
		<div id="main">
			

			

			<?php while(have_posts()) : ?>
				<?php the_post(); ?>


				
					<h2><?php the_title(); ?></h2>

					
					<?php the_content(); ?>

			<?php endwhile; ?>
		</div>
		<div id="secondary">
			<h2>Menu</h2>
			<!-- nav menu -->
			
			<?php wp_nav_menu(); ?>
			
			<h2>Archive</h2>
			<!-- getting the blogs  -->
			<ul>
				<?php wp_get_archives(); ?> 
			</ul>
		</div>
	</div>
	
</section>
<?php
	get_footer();
?>