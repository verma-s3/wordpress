	<div id="secondary" class="col-xs-12 col-sm-3">

				<?php
					$args= ['include'=>'2,11,13',
							'sort_field'=>'ID',
							'sort_order'=>'ASC'];

				 $pages= get_pages($args);?>

				 <?php foreach ($pages as $page):?>
				 	
				<div class="callout col-xs-12">
					<div class="col-xs-12">
					<a href="<?=get_the_permalink($page)?>"><?= get_the_post_thumbnail($page)?></a>
					<div class="caption  col-xs-12">
							<a href="<?=get_the_permalink($page)?>"><?=get_the_title($page) ?></a>
					</div><!-- /caption -->
					</div>
				</div><!-- /callout -->

				
			<?php endforeach;?>

			</div><!-- /secondary -->