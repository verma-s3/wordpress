<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>After School</title>

	
	<?php wp_head(); ?>
</head>
<body class="page-template-default">
	<div class="container">

		<header class="main col-xs-12">

			<span class="site_title">After School</span>

			<nav id="util">
				<ul class="menu">
					<li><a href="#">About</a></li>
					<li><a href="#">Contact</a></li>
					<li><a href="#">News</a></li>
				</ul>
			</nav>

			<a class="menu_toggle" href="#">Menu</a>

		</header>



		<nav id="main" class="col-xs-12">

			<ul class="menu">
				<li><a href="#">Home</a></li>
				<li class="current_page"><a href="#">Elementary School</a></li>
				<li><a href="#">Middle School</a></li>
				<li><a href="#">High School</a></li>
				<li><a href="#">Register</a></li>
			</ul>

		</nav>