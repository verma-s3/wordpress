<?php get_header() ?>
    <section>
      <div id="inner">
        <div id="main">
          <?php while(have_posts()) : ?>
            <?php the_post(); ?>
              <h2><?php the_title(); ?></h2>
              <p><small>Posted on <?php echo get_the_date('',$post->ID) ?>, by <?php the_author(); ?></small></p>
              <p>Posted in <?php the_category(',') ?></p>
            <?php the_content(); ?>
          <?php endwhile; ?>
          <hr />
          <?php comments_template() ?>
        </div>
        <?php get_sidebar() ?>
        
      </div>
    </section>
<?php get_footer() ?>   