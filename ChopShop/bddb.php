<?php get_header() ?>
    <section>
      <div id="inner">
        <div id="main">
          <h1>Home</h1>
          <p>Proident quis voluptate proident ea fugiat culpa dolore ut dolore occaecat dolor fugiat nostrud nisi quis cupidatat pariatur adipisicing elit deserunt amet dolor consectetur pariatur esse esse officia ut aliqua ex minim mollit esse veniam irure quis duis ullamco occaecat nulla et est deserunt anim laborum laborum voluptate ullamco eiusmod dolor anim irure pariatur cupidatat in est sed duis in elit labore ea veniam fugiat amet nulla nostrud officia quis dolor cupidatat mollit excepteur sunt ea dolore id veniam aliquip consequat ut mollit tempor excepteur ex tempor dolore laborum proident esse nisi in tempor ad qui dolor anim ex dolor sed cillum mollit ex fugiat sit qui laboris est aliquip in consequat incididunt laboris amet pariatur minim veniam anim quis sed mollit non pariatur sit eiusmod sit culpa dolore duis officia anim ea qui esse enim incididunt consectetur ut sint dolor culpa proident.</p>
          <form>
          <legend>Leave a Reply</legend>
          <p>
            <input type="text" name="name" id="name">
            <label for="name">Name(required)</label>
          </p>
          <p>
            <input type="text" name="mail" id="mail">
            <label for="mail">Mail(will not be published)(required)</label>
          </p>
          <p>
            <input type="text" name="web" id="web">
            <label for="web">Website</label>
          </p>
          <button>Submit Comment</button>
        </form>
        </div>
        <?php get_sidebar() ?>
        
      </div>
    </section>
<?php get_footer() ?>   