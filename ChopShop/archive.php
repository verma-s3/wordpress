<?php
	get_header();
?>
<section>
	
	<div id="inner">
		<div id="main">
			<h1><?=get_the_archive_title() ?></h1>
			<?php while(have_posts()) : ?>
				<?php the_post(); ?>
					<h1><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h1>
					<p><small>Posted on <?php echo get_the_date('',$post->ID) ?>, by <?php the_author(); ?></small></p>
					<p>Posted in <?php the_category(',') ?></p>
				<?php if(is_page() || is_single()): ?>
					<?php the_content(); ?>
				<?php else: ?>
					<?php the_excerpt(); ?>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>
		<div id="secondary">
			<h2>Menu</h2>
			<!-- nav menu -->			
			<?php wp_nav_menu(); ?>			
			<h2>Archive</h2>
			<!-- getting the blogs  -->
			<ul>
				<?php wp_get_archives(); ?> 
			</ul>
		</div>
	</div>	
</section>
<?php
	get_footer();
?>