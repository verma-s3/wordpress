<?php get_header() ?>
    <section>
      <div id="inner">
        <div id="main">
          <?php while(have_posts()) : ?>
            <?php the_post(); ?>
              <h2><?php the_title(); ?></h2>
          <?php the_content(); ?>

      <?php endwhile; ?>
          
        </div>
        <?php get_sidebar('page'); ?>
        
      </div>
    </section>
<?php get_footer() ?>   