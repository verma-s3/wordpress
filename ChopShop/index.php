<?php get_header() ?>
    <section>
      <div id="inner">
        <div id="main">
          <h1>Recent Posts</h1>
          <?php while(have_posts()) : ?>
            <?php the_post(); ?>
            
              
              <h2><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h2>

            
              <p><small>Posted on <?php echo get_the_date('',$post->ID) ?>, by <?php the_author(); ?></small></p>
              <p>Posted in <?php the_category(',') ?></p>

           
              <?php the_excerpt(); ?>

          <?php endwhile; ?>
          <!-- pagination link added to go over posts -->
          <?=paginate_links()?>
        </div>
        <?php get_sidebar() ?>



      </div>
    </section>
<?php get_footer() ?>   