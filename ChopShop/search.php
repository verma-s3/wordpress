<?php get_header() ?>
    <section>
      <div id="inner">
        <div id="main">
          <h1>You Search for: <?=get_search_query()?></h1>
          <?php while(have_posts()) : ?>
            <?php the_post(); ?>
            <?php if(is_page() || is_single()): ?>
              <h2 style="text-decoration: underline;"><?php the_title(); ?></h2>
            <?php else: ?>
              <h1><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h1>
            <?php endif; ?>

            <?php if(is_single()): ?>
              <p><small>Posted on <?php echo get_the_date('',$post->ID) ?>, by <?php the_author(); ?></small></p>
              <p>Posted in <?php the_category(',') ?></p>
            <?php endif; ?>

            <?php if(is_page() || is_single()): ?>
              <?php the_content(); ?>
            <?php else: ?>
              <?php the_excerpt(); ?>
            <?php endif; ?>

          <?php endwhile; ?>
          <!-- pagination link added to go over posts -->
          <?=paginate_links()?>
        </div>
        <?php get_sidebar() ?>



      </div>
    </section>
<?php get_footer() ?>   