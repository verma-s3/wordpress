<?php get_header() ?>
    <section>
      <div id="inner">
        <div id="main">
          <h1>404 | Page Not Found</h1>
          <p>The page you are looking for doesnot exist</p>
        </div>
        
        <div id="secondary">
          <form>
            <label for="serach">Search bar</label>
            <input type="search" name="search" id="search">
            <button>Seacrh</button>
          </form>
          <h2>Menu</h2>
          <?php wp_nav_menu(); ?>
          
          <h2>Archive</h2>
          <!-- getting the blogs  -->
			<ul>
				<?php wp_get_archives(); ?> 
			</ul>
        </div>
      </div>
    </section>
<?php get_footer() ?>   