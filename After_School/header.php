<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>After School</title>

	
	<?php wp_head(); ?>
</head>
<body<?=body_class() ?>>
	<div class="container">

		<header class="main col-xs-12">

			<span class="site_title">After School</span>

			<nav id="util">
				<?php afterschool_util_menu() ?>
			</nav>

			<a class="menu_toggle" href="#">Menu</a>

		</header>



		<nav id="main" class="col-xs-12">
			<?php afterschool_main_menu() ?>
		</nav>