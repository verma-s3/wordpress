<?php get_header(); ?>

		<div id="content" class="col-xs-12">


			<div id="primary" class="col-xs-12 col-sm-9">

				

				<?php if(have_posts()):?>

				<?php while(have_posts()): the_post(); ?>

					 <article>

						<h1><?php the_title() ?></h1>
						<span><?php the_date() ?></span>
						<p><?php the_content(); ?></p>

					</article>
				<?php endwhile; ?>
				<?php endif; ?>
			</div><!-- /primary -->

		<?php get_sidebar(); ?>

		</div><!-- /content -->

<?php get_footer(); ?>